#ifndef _APPLICATION_H
#define _APPLICATION_H

#include <bcl.h>

#define SWITCH_TO_NORMAL_MODE_MS (5 * 60 * 1000) // 10 minutes
#define AUTOMATIC_RESTART_INTERVAL_MS (48 * 60 * 60 * 1000) // 24 hours

#define BATTERY_INIT_UPDATE_INTERVAL_MS   (1 * 60 * 1000) // 1 min
#define BATTERY_UPDATE_INTERVAL_MS   (10 * 60 * 1000) // 10 min

#define TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS (10 * 60 * 1000) // 10 min
#define TEMPERATURE_PUB_VALUE_CHANGE 0.2f
#define TEMPERATURE_INIT_UPDATE_INTERVAL_MS (1 * 60 * 1000) // 1 min
#define TEMPERATURE_UPDATE_INTERVAL_MS (10 * 60 * 1000) // 10 min

#define WATER_LEVEL_PUB_NO_CHANGE_INTERVAL_MS (10 * 60 * 1000) // 10 min
#define WATER_LEVEL_PUB_VALUE_CHANGE 1.0f
#define WATER_LEVEL_INIT_UPDATE_INTERVAL_MS (1 * 60 * 1000) // 1 min
#define WATER_LEVEL_UPDATE_INTERVAL_MS (10 * 60 * 1000) // 10 min

#define WATER_LEVEL_SENSOR_DISTANCE_FROM_BOTTOM_CM 190
#define WATER_TANK_AREA_M2 1.9 * 1.7

typedef struct
{
    uint8_t channel;
    float value;
    bc_tick_t next_pub;

} event_param_t;

#ifndef VERSION
#define VERSION "dev"
#endif

#ifndef FIRMWARE
#define FIRMWARE "bigclown-water-tank-sensor"
#endif

#endif
