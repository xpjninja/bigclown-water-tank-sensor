#include <application.h>
#include <bc_hc_sr04.h>
#include <SPIRIT_Qi.h>

bc_led_t led;
bc_tmp112_t tmp112;

static event_param_t temperature_event_param = { .next_pub = 0 };
static event_param_t water_level_event_param = { .next_pub = 0 };

bc_tick_t current_water_level_update_interval = WATER_LEVEL_INIT_UPDATE_INTERVAL_MS;
bc_scheduler_task_id_t water_sensor_read_task_id;
bc_scheduler_task_id_t water_sensor_read_prep_task_id;

void restart()
{
    bc_log_info("reset required");
    bc_system_reset();
}

float round_d(double d) {
    return floor(d + 0.5);
}

void send_water_level_data(void *event_param)
{
    event_param_t *param = (event_param_t *)event_param;
    float distance_cm = round_d(param->value / 10.0);
    float water_level_cm = WATER_LEVEL_SENSOR_DISTANCE_FROM_BOTTOM_CM - distance_cm;
    float content_m3 = WATER_TANK_AREA_M2 * round_d(water_level_cm / 100.0);

    bc_log_info("Sending water level values: Distance %f cm, water level %f cm, content %f m3", distance_cm, water_level_cm, content_m3);

    bc_radio_pub_float("water-tank/a/dist-cm", &distance_cm); //distance from top
    bc_radio_pub_float("water-tank/a/level-cm", &water_level_cm); // water level cm
    bool b = bc_radio_pub_float("water-tank/a/cont-m3", &content_m3);
    bc_log_info("Succesfully sent %d", b);
}

void battery_event_handler(bc_module_battery_event_t event, void *event_param)
{
    (void) event_param;

    float voltage;

    if (event == BC_MODULE_BATTERY_EVENT_UPDATE)
    {
        if (bc_module_battery_get_voltage(&voltage))
        {
            bc_radio_pub_battery(&voltage);

            float rssi_dbm = SpiritQiGetRssidBm();

            bc_log_info("RSSI dBm: %f", rssi_dbm);
            bc_radio_pub_float("water-tank/a/rssi-dbm", &rssi_dbm);
        }
    }
}

void tmp112_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param)
{
    float value;
    event_param_t *param = (event_param_t *)event_param;

    if (event == BC_TMP112_EVENT_UPDATE)
    {
        if (bc_tmp112_get_temperature_celsius(self, &value))
        {
            if ((fabsf(value - param->value) >= TEMPERATURE_PUB_VALUE_CHANGE) || (param->next_pub < bc_scheduler_get_spin_tick()))
            {
                bc_radio_pub_temperature(param->channel, &value);

                param->value = value;
                param->next_pub = bc_scheduler_get_spin_tick() + TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS;
            }
        }
    }
}


void hc_sr04_event_handler(bc_hc_sr04_event_t event, void *event_param)
{
    event_param_t *param = (event_param_t *)event_param;

    float value;
    bc_log_info("Reading distance...");

    if (event != BC_HC_SR04_EVENT_UPDATE)
    {
        bc_log_info("Problem with distance update");
        return;
    }

    if (bc_hc_sr04_get_distance_millimeter(&value))
    {
        bc_log_info("Distance update %f mm", value);
        if ((fabsf(value - param->value) >= WATER_LEVEL_PUB_VALUE_CHANGE) || (param->next_pub < bc_scheduler_get_spin_tick())) {
            bc_log_info("Distance: %f mm", value);

            param->value = value;
            param->next_pub = bc_scheduler_get_spin_tick() + WATER_LEVEL_PUB_NO_CHANGE_INTERVAL_MS;

            send_water_level_data(param);
        }

    }
    bc_led_set_mode(&led, BC_LED_MODE_OFF);
    bc_gpio_set_output(BC_GPIO_P7, 0);
}

void water_sensor_read() {
    bc_log_info("Measuring distance...");

    bc_hc_sr04_measure();
}

void water_sensor_read_prep() {
    bc_log_info("Starting distance sensor...");
    bc_led_set_mode(&led, BC_LED_MODE_ON);

    bc_gpio_set_output(BC_GPIO_P7, 1);

    bc_scheduler_plan_from_now(water_sensor_read_task_id, 2000);
    bc_scheduler_plan_from_now(water_sensor_read_prep_task_id, current_water_level_update_interval);
}

void switch_to_normal_mode() {
    bc_module_battery_set_update_interval(BATTERY_UPDATE_INTERVAL_MS);
    bc_tmp112_set_update_interval(&tmp112, TEMPERATURE_UPDATE_INTERVAL_MS);

    current_water_level_update_interval = WATER_LEVEL_UPDATE_INTERVAL_MS;
}

void __unused application_init(void)
{
    bc_log_init(BC_LOG_LEVEL_DUMP, BC_LOG_TIMESTAMP_ABS);
    bc_log_info("Initializing Water tank sensor module");

    bc_led_init(&led, BC_GPIO_LED, false, false);
    bc_led_set_mode(&led, BC_LED_MODE_OFF);

    bc_radio_init(BC_RADIO_MODE_NODE_SLEEPING);
    bc_radio_pairing_request("water-tank-sensor", VERSION);

    bc_module_battery_init();
    bc_module_battery_set_event_handler(battery_event_handler, NULL);
    bc_module_battery_set_update_interval(BATTERY_INIT_UPDATE_INTERVAL_MS);

    temperature_event_param.channel = BC_RADIO_PUB_CHANNEL_R1_I2C0_ADDRESS_ALTERNATE;
    bc_tmp112_init(&tmp112, BC_I2C_I2C0, 0x49);
    bc_tmp112_set_event_handler(&tmp112, tmp112_event_handler, &temperature_event_param);
    bc_tmp112_set_update_interval(&tmp112, TEMPERATURE_INIT_UPDATE_INTERVAL_MS);

    bc_gpio_init(BC_GPIO_P7);
    bc_gpio_set_mode(BC_GPIO_P7, BC_GPIO_MODE_OUTPUT);

    bc_hc_sr04_init();
    bc_hc_sr04_set_event_handler(hc_sr04_event_handler, &water_level_event_param);
    water_sensor_read_prep_task_id = bc_scheduler_register(water_sensor_read_prep, NULL, current_water_level_update_interval);
    water_sensor_read_task_id = bc_scheduler_register(water_sensor_read, NULL, BC_TICK_INFINITY);

    bc_scheduler_register(restart, NULL, AUTOMATIC_RESTART_INTERVAL_MS);
    bc_scheduler_register(switch_to_normal_mode, NULL, SWITCH_TO_NORMAL_MODE_MS);

    bc_led_pulse(&led, 2000);
    bc_log_info("Water tank sensor module initialized");
}

void __unused application_task(void)
{

}
